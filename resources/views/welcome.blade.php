@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome</div>

                <div class="panel-body">
                    Please <a href="/login">log in</a> or <a href="/register">sign up</a>!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
