<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <title><?= Auth::user()->name ?> To-dos</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" />

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="css/styles.css" />
    <!-- Scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular.min.js"></script>
    <script src="/js/todo.js"> </script>
</head>
<body ng-app="todoApp">
    <nav>
        <a href="<?= url('/logout') ?>"><i class="fa fa-sign-out"></i>Logout</a>
    </nav>

    <h1><?= Auth::user()->name ?>'s Task List</h1>
    <section ng-controller="TodoListController as todoList" class="todo-area">
        <ul class="todo-items">
            <li ng-repeat="todo in todoList.todos" ng-click="todoList.complete(todo.id)">
                {{todo.task}}
            </li>
        </ul>

        <form class="add-item" ng-submit="todoList.addTodo()">
            <input type="text" ng-model="todoList.todoText" maxlength="512" placeholder="Add new todo here. Press enter to submit.">
            <button type="submit" class="action-button">Add</button>
        </form>
    </section>
</body>
</html>