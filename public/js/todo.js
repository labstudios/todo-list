
angular.module('todoApp', []).controller('TodoListController', function($http) {
  var todoList = this;
  todoList.todos = [];
  
  todoList.refresh = function(){
    $http.get("/api/tasks").then(function(dt){
      if(dt.data.success){
        todoList.todos = dt.data.tasks;
      }
    });
  };

  todoList.addTodo = function() {
    console.log("adding todo",todoList.todoText);
    $http.post("/api/task", {
        task: todoList.todoText
      }).then(function(dt){
        if(dt.data.success){
          todoList.todoText = "";
          todoList.refresh();
        }else{
          alert(dt.data.message);
        }
      });
  };
 
  todoList.complete = function(id) {
    $http.delete("/api/task/" + id).then(function(dt){
      console.log(dt.data.success);
      todoList.refresh();
    });
  };

  todoList.refresh();
});