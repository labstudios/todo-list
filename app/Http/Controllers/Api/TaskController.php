<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TaskController extends Controller
{
    private $responseData = [
        "status" => "failure",
        "success" => false,
        "message" => "An unknown error has occurred."
    ];

    public function getTasks(){
        $this->responseData = (Object) $this->responseData;

        $user = Auth::user();
        $tasks = $user->tasks()->orderBy('created_at', 'asc')->get();
        $this->responseData->tasks = [];
        foreach($tasks as $task){
            $this->responseData->tasks[] = $task->objectify();
        }
        if(count($this->responseData->tasks)){
            $this->setSuccess();
        } else{
           $this->setSuccess("No tasks available");
        }
        return response()->json($this->responseData);
    }

    public function getTask(Task $task){
        $this->responseData = (Object) $this->responseData;

        if(!empty($task->id)){
            $this->responseData->task = $task->objectify();
            $this->setSuccess();
        }

        return response()->json($this->responseData);
    }

    public function createTask(Request $request){

        $user = Auth::user();

        if(empty($request->get("task"))){
            $this->responseData["message"] = "No content available to add";
            return response()->json($this->responseData);
        }

        $task = new Task();
        $task->user_id = $user->id;
        $task->task = $request->get("task");
        $task->save();

        if(!empty($task->id)){
            $this->responseData["task"] = $task->objectify();
            $this->setSuccess("Task successfully created");
        }
        return response()->json($this->responseData);
    }

    public function deleteTask($id){
        $this->responseData = (Object) $this->responseData;
        $task = Task::find($id);

        if($task && $task->user_id == Auth::user()->id){
            $task->delete();
            $this->setSuccess("Task \"$task->task\" deleted.");
        }else{
            $this->responseData->message = "Task not found for this user.";
        }

        return response()->json($this->responseData);
    }

    private function setSuccess($message = "Success"){
        if(!is_object($this->responseData)){
            $this->responseData = (Object) $this->responseData;
        }

        $this->responseData->status = "success";
        $this->responseData->success = true;
        $this->responseData->message = $message;
    }
}