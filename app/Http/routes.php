<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::get("/app", ["as" => "app", "uses"=>"AppController@index", "middleware" => ['App\Http\Middleware\Authenticate'] ]);
Route::get("/app/test", ["as" => "app::test", "uses"=>"AppController@getTasks", "middleware" => ['App\Http\Middleware\Authenticate'] ]);

Route::group(["as" => "api::", "middleware" => ['App\Http\Middleware\ApiVerification'], "prefix" => "api", "namespace" => "Api"], function(){
	Route::get("tasks", ["as" => "taskget", "uses" => "TaskController@getTasks"]);
	Route::get("task/{id}", ["as" => "get-task", "uses" => "TaskController@getTasks"]);
	Route::post("task", ["as" => "task::create", "uses" => "TaskController@createTask"]);
	Route::delete("task/{id}", ["as" => "task::delete", "uses" => "TaskController@deleteTask"]);
});