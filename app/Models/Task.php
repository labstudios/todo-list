<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{
	use SoftDeletes;

	protected $table = "tasks";
	protected $guarded = ['id'];

	public function user(){
		return $this->hasOne("App\User");
	}

	public function objectify(){
		$obj = [
			"id" => $this->id,
			"task" => $this->task,
			"created" => $this->created_at
		];
		return (Object) $obj;
	}

}